set heading off pages 50000 feedback off verify off lines 200
select 'alter database datafile '''||file_name||''' resize 2g;'
from dba_data_files
order by tablespace_name,file_id;

select 'alter database tempfile '''||file_name||''' resize 2g;'
from dba_temp_files
order by tablespace_name,file_id;

select 'alter database datafile '''||file_name||''' autoextend on maxsize 4g;'
from dba_data_files
order by tablespace_name,file_id;

select 'alter database tempfile '''||file_name||''' autoextend on maxsize 4g;'
from dba_temp_files
order by tablespace_name,file_id;