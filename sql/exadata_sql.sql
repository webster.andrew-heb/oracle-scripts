SELECT sql_text,
       io_cell_offload_eligible_bytes/1024/1024 cell_offload_eligible_mb,
       io_cell_uncompressed_bytes/1024/1024 io_uncompressed_mb,
       io_interconnect_bytes/1024/1024 io_interconnect_mb,
       io_cell_offload_returned_bytes/1024/1024 cell_return_bytes_mb,
       (physical_read_bytes + physical_write_bytes)/1024/1024 io_disk_mb 
       FROM v$sql
where sql_id = '8agvc83tt9rxq';