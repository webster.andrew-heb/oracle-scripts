col property_value format a30
select PROPERTY_NAME,property_value
from database_properties
where property_name in ('NLS_CHARACTERSET','NLS_NCHAR_CHARACTERSET')
order by 1;