set lines 150
set numwidth 3
select arc_day,
nvl(max(decode(arc_hour,'00',num_logs)),0) "0",
nvl(max(decode(arc_hour,'01',num_logs)),0) "1",
nvl(max(decode(arc_hour,'02',num_logs)),0) "2",
nvl(max(decode(arc_hour,'03',num_logs)),0) "3",
nvl(max(decode(arc_hour,'04',num_logs)),0) "4",
nvl(max(decode(arc_hour,'05',num_logs)),0) "5",
nvl(max(decode(arc_hour,'06',num_logs)),0) "6",
nvl(max(decode(arc_hour,'07',num_logs)),0) "7",
nvl(max(decode(arc_hour,'08',num_logs)),0) "8",
nvl(max(decode(arc_hour,'09',num_logs)),0) "9",
nvl(max(decode(arc_hour,'10',num_logs)),0) "10",
nvl(max(decode(arc_hour,'11',num_logs)),0) "11",
nvl(max(decode(arc_hour,'12',num_logs)),0) "12",
nvl(max(decode(arc_hour,'13',num_logs)),0) "13",
nvl(max(decode(arc_hour,'14',num_logs)),0) "14",
nvl(max(decode(arc_hour,'15',num_logs)),0) "15",
nvl(max(decode(arc_hour,'16',num_logs)),0) "16",
nvl(max(decode(arc_hour,'17',num_logs)),0) "17",
nvl(max(decode(arc_hour,'18',num_logs)),0) "18",
nvl(max(decode(arc_hour,'19',num_logs)),0) "19",
nvl(max(decode(arc_hour,'20',num_logs)),0) "20",
nvl(max(decode(arc_hour,'21',num_logs)),0) "21",
nvl(max(decode(arc_hour,'22',num_logs)),0) "22",
nvl(max(decode(arc_hour,'23',num_logs)),0) "23"
from (
select count(1) num_logs,trunc(completion_time) arc_day,to_char(completion_time,'HH24') arc_hour
from v$archived_log
where dest_id = 1
and completion_time >= trunc(sysdate - 7)
group by trunc(completion_time),to_char(completion_time,'HH24'))
group by arc_day
order by 1;