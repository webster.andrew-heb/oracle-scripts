set lines 150
set echo off
set verify off

select username,profile,default_tablespace,temporary_tablespace
from dba_users
where username = upper('&&username');

select tablespace_name,ceil(sum(bytes) / 1048576) mb
from dba_segments
where owner = upper('&&username')
group by tablespace_name
order by 1;

select owner,table_name,privilege,grantee
from dba_tab_privs
where owner = 'SYS'
and grantee = upper('&&username');

select distinct grantee needed_roles
from dba_tab_privs
where owner = upper('&&username')
and grantee in (select role from dba_roles);

