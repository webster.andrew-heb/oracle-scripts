  select a.name,
         a.rule_expr
  from dvsys.dba_dv_rule a
  order by a.name;
  
select *
from dvsys.DBA_DV_RULE_SET_RULE
where rule_set_name = 'Allow Sessions'
order by 2;
  
select * from dvsys.audit_trail$
where timestamp > trunc(sysdate)
order by timestamp desc;