create table oracle.rtest (
id integer not null,
date1 date default sysdate,
col1nn varchar2(30) not null,
col2 varchar2(30)) tablespace data;

alter table oracle.rtest add constraint rtest_pk primary key (id);
create trigger oracle.rtest_trg
before insert on oracle.rtest
begin
  case
   when inserting then
     dbms_output.put_line('inserting');
   end case;
end;
/

set serveroutput on
insert into oracle.rtest (id,col1nn,col2) values (1,'something','another something');