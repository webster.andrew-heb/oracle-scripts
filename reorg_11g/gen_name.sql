set serveroutput on
declare
    g_owner varchar2(30) := 'ANDY';
    g_tabname varchar2(30) := 'SHORT_TABNAME_XXXXXXXXXXXXXxxx';
    function get_interim_name(l_owner varchar2,l_tabname varchar2) return varchar2
    is
      l_length integer;
    begin
        l_length := length(l_tabname);
        if l_length < 28 then
            return l_tabname;
        else
            raise_application_error(-20000,'table name is too long, needs to be less than 28 characters');
        end if;
    end get_interim_name;
begin
    dbms_output.put_line('interim name '||get_interim_name(g_owner,g_tabname));
end;
/
