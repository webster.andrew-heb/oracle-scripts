set verify off
define sowner=&1
define stabname=&2
define sitabname=&3
define stablespace=&4

select 'reorging &&sowner..&&stabname using &&sowner..&&sitabname' msg,to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') start_time from dual;

declare
    g_owner varchar2(30) := upper('&&sowner');
    g_tabname varchar2(30) := upper('&&stabname');
    g_i_tabname varchar2(30) := upper('&&sitabname');
    g_tablespace varchar2(30) := upper('&&stablespace');

    procedure can_redef(l_owner varchar2,l_tabname varchar2)
    is
      v_count integer;
    begin
        select count(*) into v_count from dba_part_tables where owner = l_owner and table_name = l_tabname;
        if  v_count > 0 then
            raise_application_error(-20000,'we are unable to reorg a partitioned table');
        end if;

        dbms_redefinition.can_redef_table(l_owner,l_tabname);

    exception
        when others then
            raise_application_error(-20000,'can_redef: '||dbms_utility.format_error_stack);
    end can_redef;

    procedure create_interim_table(l_owner varchar2,l_tabname varchar2,l_interim varchar2,l_tablespace varchar2)
    is
        l_mdh number;
        l_th number;
        l_ddlh number;
        l_ddl clob;
    begin
        l_mdh := dbms_metadata.open('TABLE');
        dbms_metadata.set_filter(l_mdh,'SCHEMA',l_owner);
        dbms_metadata.set_filter(l_mdh,'NAME',l_tabname);
        l_th := dbms_metadata.add_transform(l_mdh,'MODIFY');
        dbms_metadata.set_remap_param(l_th,'REMAP_NAME',l_tabname,l_interim);
        l_ddlh := dbms_metadata.add_transform(l_mdh,'DDL');
        dbms_metadata.set_transform_param(l_ddlh,'STORAGE',false);
        dbms_metadata.set_transform_param(l_ddlh,'TABLESPACE',false);
        dbms_metadata.set_transform_param(l_ddlh,'CONSTRAINTS',false);
        dbms_metadata.set_transform_param(l_ddlh,'REF_CONSTRAINTS',false);
        l_ddl := dbms_metadata.fetch_clob(l_mdh)||' tablespace '||l_tablespace;
        dbms_metadata.close(l_mdh);
        execute immediate l_ddl;
    exception
        when others then
            raise_application_error(-20000,'create_interim_table: '||dbms_utility.format_error_stack);
    end create_interim_table;

    procedure start_redef(l_owner varchar2,l_tabname varchar2,l_interim varchar2)
    is
    begin
        dbms_redefinition.start_redef_table(l_owner,l_tabname,l_interim);
    exception
        when others then
            raise_application_error(-20000,'start_redef: '||dbms_utility.format_error_stack);
    end start_redef;

    procedure copy_dependents(l_owner varchar2,l_tabname varchar2,l_interim varchar2)
    is
        l_errors integer;
    begin
        dbms_redefinition.copy_table_dependents(l_owner,l_tabname,l_interim,ignore_errors => false,num_errors => l_errors);
    exception
        when others then
            raise_application_error(-20000,'copy_dependents: '||dbms_utility.format_error_stack);
    end copy_dependents;
    
    procedure finish_redef(l_owner varchar2,l_tabname varchar2,l_interim varchar2)
    is
    begin
        dbms_redefinition.finish_redef_table(l_owner,l_tabname,l_interim);
    exception
        when others then
            raise_application_error(-20000,'finish_redef: '||dbms_utility.format_error_stack);
    end finish_redef;

    procedure gather_stats(l_owner varchar2,l_tabname varchar2)
    is
    begin
        dbms_stats.gather_table_stats(l_owner,l_tabname);
    exception
        when others then
            raise_application_error(-20000,'gather_stats: '||dbms_utility.format_error_stack);
    end gather_stats;

begin
    can_redef(g_owner,g_tabname);
    create_interim_table(g_owner,g_tabname,g_i_tabname,g_tablespace);
    start_redef(g_owner,g_tabname,g_i_tabname);
    copy_dependents(g_owner,g_tabname,g_i_tabname);
    gather_stats(g_owner,g_i_tabname);
    finish_redef(g_owner,g_tabname,g_i_tabname);
exception
    when others then
        raise_application_error(-20000,'error reorging '||g_owner||'.'||g_tabname||' '||dbms_utility.format_error_stack);
end;
/

select 'reorging complete &&sowner..&&stabname using &&sowner..&&sitabname' msg,to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') end_time from dual;
