set longchunksize 300 long 20000
var g_ddl clob;
declare
    g_owner varchar2(30) := 'ORACLE';
    g_tabname varchar2(30) := 'RTEST';
    g_i_tabname varchar2(30) := 'RTEST_ROG';
    g_mdh number;
    g_th number;
    g_ddlh number;
begin
    g_mdh := dbms_metadata.open('TABLE');
    dbms_metadata.set_filter(g_mdh,'SCHEMA',g_owner);
    dbms_metadata.set_filter(g_mdh,'NAME',g_tabname);
    g_th := dbms_metadata.add_transform(g_mdh,'MODIFY');
    dbms_metadata.set_remap_param(g_th,'REMAP_NAME',g_tabname,g_i_tabname);
    g_ddlh := dbms_metadata.add_transform(g_mdh,'DDL');
    dbms_metadata.set_transform_param(g_ddlh,'STORAGE',false);
    dbms_metadata.set_transform_param(g_ddlh,'TABLESPACE',false);
    dbms_metadata.set_transform_param(g_ddlh,'CONSTRAINTS',true);
    dbms_metadata.set_transform_param(g_ddlh,'REF_CONSTRAINTS',false);
    :g_ddl := dbms_metadata.fetch_clob(g_mdh)||' tablespace data';
    dbms_metadata.close(g_mdh);
end;
/
print g_ddl
