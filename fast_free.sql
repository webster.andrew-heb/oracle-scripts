set lines 200
select ts.tablespace_name,round((used_space * to_number(p.value)) / 1073741824) used_gb,
round((ts.tablespace_size * to_number(p.value)) / 1073741824) tssize_gb 
from dba_tablespace_usage_metrics ts,v$parameter p
where p.name = 'db_block_size'
order by ts.used_space desc
/
