# Andy's Oracle SQL Developer Reports
You can import these into your SQL Developer.
## Enable Reports pane
1. Make sure you have the __Reports__ pane enabled.

    Click the View menu and select __Reports__
## Import Reports    
1. On the __Reports__ pane, right-click __User Defined Reports__ and click __Open Report__

    Browse to the XML file you want to import and click __Open__
    
## Reports

* ActiveSessions.xml - List active sessions.

* ASH_During_Time_Period.xml - Show database waits during a time period

* ASH_SQL_During_Time_Period.xml - Show top SQL during a time period

* ExplainPlanCursor.xml - Show Execution Plan for a SQLID

* ExplainPlanAWR.xml - Show Execution Plan for a SQLID from AWR

* SQLMonitor11g.xml - Show SQL Monitor report for 11g databases

* SQLMonitor12c.xml - Show SQL Monitor report for 12c and up

* SQLPlan_History.xml - Show SQL Plan History (plan changes)

## Misc Info

* AWR - Automatic Workload Repository - This is Oracle's long-term performance history, by default you get 7 days
* ASH - Active Session History - This is Oracle's short-term performance history.  It gives you 1 second samples, but is limited to what can be stored in memory