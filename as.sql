set lines 200
col kill_string format a15
col event format a32
col machine format a15
col username format a30
select s.sid||','||s.serial#||',@'||s.inst_id kill_string
-- ,s.inst_id
  ,s.username
  ,s.sql_id
  ,s.event
  ,s.last_call_et
  ,s.state wait_state
--  ,s.wait_time_micro
--  ,s.blocking_session_status
--  ,s.blocking_session
  ,s.machine
--  ,s.program
--  ,s.PLSQL_ENTRY_OBJECT_ID
--  ,s.blocking_instance
--  ,s.sql_child_number
--  ,s.sql_exec_start
--  ,s.logon_time
--  ,p.spid
--  ,p.tracefile
from gv$session s,gv$process p
where s.username is not null
and s.username not in ('SYS','PUBLIC')
and s.status = 'ACTIVE'
and p.addr = s.paddr
and p.inst_id = s.inst_id
order by 5 desc
/
