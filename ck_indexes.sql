set lines 200 verify off
col owner format a20
col index_name format a30
select owner,index_name,status,DOMIDX_OPSTATUS,UNIQUENESS
from dba_indexes
where table_owner = upper('&table_owner')
and table_name = upper('&table_name')
order by 1,2
/
