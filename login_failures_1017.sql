set lines 200 pages 100
col userhost format a25
col os_user format a10
col extended_timestamp format a26
col db_user format a15
select RETURNCODE,EXTENDED_TIMESTAMP,OS_USER,USERHOST,DB_USER
from dba_common_audit_trail
where returncode = 1017
order by EXTENDED_TIMESTAMP
/
