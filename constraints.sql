set linesize 132
col TABLE_NAME          format a20
col CONSTRAINT_NAME format a20
col R_CONSTRAINT_NAME   format a20
col SEARCH_CONDITION    format a30
col DELETE_RULE         format a8

select      owner,
    CONSTRAINT_NAME,
    decode (CONSTRAINT_TYPE, 'C', 'CHECK',
        'P', 'PRIMARY KEY',
        'U', 'UNIQUE',
        'R', 'REF INTEG',
        'V', 'CHK ON VIEW') TYPE,
    TABLE_NAME,
    SEARCH_CONDITION,
    R_CONSTRAINT_NAME,
    DELETE_RULE,
    STATUS
from        DBA_CONSTRAINTS
where owner = upper('&owner')
    and table_name = upper('&table_name')
order by    TABLE_NAME,
    CONSTRAINT_TYPE,
    CONSTRAINT_NAME
/


